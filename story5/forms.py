from django import forms

class MatkulForm(forms.Form):

    nama_Matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Mata Kuliah',
        'type' : 'text',
        'required' : True,
    }))

    dosen_Pengajar = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required' : True,
    }))

    jumlah_SKS = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'Integer',
        'required' : True,
    }))

    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'placeholder' : 'deskripsi matkul',
        'required' : True,
    }))

    semester_Tahun = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'Integer',
        'placeholder' : 'Semester Tahun',
        'required' : True,
    }))

    ruang_Kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'placeholder' : 'Ruang Kelas',
        'required' : True,
    }))