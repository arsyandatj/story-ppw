from django.shortcuts import render,redirect
from .models import Matkul as matkulz
from .forms import MatkulForm

# Create your views here.

def formPage(request):
    return render(request,'formPage.html')

def Join(request):
    if request.method == "POST":
        form  = MatkulForm(request.POST)
        if form.is_valid():
            mtkul = matkulz()
            mtkul.nama_Matkul = form.cleaned_data['nama_Matkul']
            mtkul.dosen_Pengajar = form.cleaned_data['dosen_Pengajar']
            mtkul.jumlah_SKS = form.cleaned_data['jumlah_SKS']
            mtkul.deskripsi = form.cleaned_data['deskripsi']
            mtkul.semester_Tahun = form.cleaned_data['semester_Tahun']
            mtkul.ruang_Kelas = form.cleaned_data['ruang_Kelas']
            mtkul.save()
        return redirect('/formPage')
    else:
        mtkul = matkulz.objects.all()
        form = MatkulForm()
        response = {'mtkul':mtkul, 'form':form}
        return render(request,'formPage.html',response)

def mtkul_delete(request,pk):
    if request.method == "POST":
        form  = MatkulForm(request.POST)
        if form.is_valid():
            mtkul = matkulz()
            mtkul.nama_Matkul = form.cleaned_data['nama_Matkul']
            mtkul.dosen_Pengajar = form.cleaned_data['dosen_Pengajar']
            mtkul.jumlah_SKS = form.cleaned_data['jumlah_SKS']
            mtkul.deskripsi = form.cleaned_data['deskripsi']
            mtkul.semester_Tahun = form.cleaned_data['semester_Tahun']
            mtkul.ruang_Kelas = form.cleaned_data['ruang_Kelas']
            mtkul.save()
        return redirect('/formPage')
    else:
        matkulz.objects.filter(pk=pk).delete()
        data = matkulz.objects.all()
        form = MatkulForm()
        response = {"mtkul":data, "form": form}
        return render(request,'formPage.html', response)
        