from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama_Matkul = models.CharField(max_length=50, null = True)
    dosen_Pengajar = models.CharField(max_length=50, null = True)
    jumlah_SKS = models.IntegerField(null = True)
    deskripsi = models.CharField(max_length=100)
    semester_Tahun = models.IntegerField(null = True)
    ruang_Kelas = models.CharField(max_length=50, null = True)

