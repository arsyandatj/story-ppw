from django.urls import path
from .views import Join, mtkul_delete

app_name = 'story5'

urlpatterns = [
    # path('formPage/',views.formPage,name='formPage'),
    path('',Join, name = 'formPage'),
    path('<int:pk>', mtkul_delete, name ='delete')
]