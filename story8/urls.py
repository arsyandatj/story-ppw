from django.urls import path

from . import views

app_name = 'story8'

urlpatterns= [
    path('', views.home, name = "home" ),
    path('searchResult/<str:key>', views.searchView, name="search")
]
