from django.test import TestCase, Client
from django.urls import resolve, reverse
from story8.views import *


class UnitTesting (TestCase):
    def test_urlhomeexist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home1.html')

    def test1_viewsTestJson(self):
        response = Client().get('/story8/searchResult/test')
        self.assertContains(response, "https://www.googleapis.com")


