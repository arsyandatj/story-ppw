from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.tambahKegiatan, name='kegiatan'),
    path('data/', views.dataKegiatan, name='dataKegiatan'),
    # dilanjutkan ...
]