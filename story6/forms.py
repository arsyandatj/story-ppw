from django.forms import ModelForm
from .models import Kegiatan, Pendaftar

class KegiatanForm(ModelForm):
    required_css_class = 'required'

    class Meta :
        model = Kegiatan
        fields = '__all__'   

class PendaftarForm(ModelForm):
    required_css_class = 'required'

    class Meta :
        model = Pendaftar
        fields = '__all__'   
