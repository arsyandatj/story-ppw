from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from story6.models import *
from story6.forms import *
from story6.views import *

# Create your tests here.

class Story6Test(TestCase):
    
    def test_story6app_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code,200)

    def test_template_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan.html')

    def test_template_datakegiatan(self):
        response = Client().get('/kegiatan/data/')
        self.assertTemplateUsed(response, 'dataKegiatan.html')

    def test_models_create(self):
        k1 = Kegiatan.objects.create(kegiatan="Golf")
        Pendaftar.objects.create(kegiatan = k1, nama="me")
        hitungJumlah = Kegiatan.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_new_Pendaftar_form_validation(self):
        kegiatan_new = Kegiatan.objects.create(kegiatan = "Basket")
        form = PendaftarForm(data = {'kegiatan' :kegiatan_new, 'nama' : 'Unit testing'})
        self.assertTrue(form.is_valid())

    def test_data_view_show(self):
        Kegiatan.objects.create(kegiatan='Test')
        request = HttpRequest()
        response = dataKegiatan(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Test', isi_html)

    def test_activity_add_using_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, tambahKegiatan)


    def test_insert_blank_Pendaftar(self):
        kegiatan_new = Kegiatan.objects.create(kegiatan = "Golf")
        form = PendaftarForm(data = {'kegiatan' :kegiatan_new, 'nama' : ''})
        self.assertEqual(form.errors['nama'], ["This field is required."])

    def test_model_name(self):
        Kegiatan.objects.create(kegiatan='Test')
        kegiatan = Kegiatan.objects.get(kegiatan='Test')
        self.assertEqual(str(kegiatan),'Test')

    def test_model_relational(self):
        Kegiatan.objects.create(kegiatan = "owo")
        kegiatan_id = Kegiatan.objects.all()[0].id
        kegiatan_di_id = Kegiatan.objects.get(id=kegiatan_id)
        Pendaftar.objects.create(kegiatan = kegiatan_di_id, nama="aa")
        jumlah = Pendaftar.objects.filter(kegiatan=kegiatan_di_id).count()
        self.assertEquals(jumlah, 1)








        

    
