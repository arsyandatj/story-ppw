from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    kegiatan = models.CharField('Kegiatan', max_length=100, null=True, blank=False)

    class Meta:
        db_table = 'kegiatan'

    def __str__(self):
        return str(self.kegiatan)


class Pendaftar(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama = models.CharField('Pendaftar', max_length=50, null=True, blank=False)

    class Meta:
        db_table = 'pendaftar'

    def __str__(self):
        return str(self.nama)

