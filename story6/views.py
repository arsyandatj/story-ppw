from django.shortcuts import redirect, render
from django.http import HttpResponse
from story6.models import Kegiatan, Pendaftar
from story6.forms import KegiatanForm, PendaftarForm
# Create your views here.

def kegiatan(request):
    return render(request,'kegiatan.html')

# Create your views here.
def tambahKegiatan(request):
    data_kegiatan_list = Kegiatan.objects.all()
    form = KegiatanForm()
    if request.method == 'POST':
        forminput = KegiatanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'kegiatan.html', {'form':form, 'status': 'success', 'data' : data_kegiatan_list})
        else:
            return render(request, 'kegiatan.html', {'form':form, 'status': 'failed', 'data' : data_kegiatan_list})
            
    else: #Kalo method GET
        current_data = Kegiatan.objects.all()
        return render(request, 'kegiatan.html', {'form':form, 'data':current_data})

def dataKegiatan(request):
    form = PendaftarForm()
    data_kegiatan_list = Kegiatan.objects.all()
    data_pendaftar_list = Pendaftar.objects.all()
    if request.method == 'POST':
        forminput = PendaftarForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return redirect('story6:dataKegiatan')
        else:
            dataPendaftar = Pendaftar.objects.all()
            return render(request, 'dataKegiatan.html', {'form':form, 'status': 'failed', 'dataPendaftar' : data_pendaftar_list, 'dataKegiatan': data_kegiatan_list,})
    else:
        return render(request,'dataKegiatan.html', {'form':form, 'status': 'failed', 'dataPendaftar' : data_pendaftar_list, 'dataKegiatan': data_kegiatan_list})


