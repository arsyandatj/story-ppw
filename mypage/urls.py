from django.urls import path
from . import views

app_name = 'mypage'

urlpatterns = [
    path('', views.index, name='index'),
    path('about_me/', views.about_me, name='about_me'),
    path('skill/', views.skill, name='skill'),
    path('gallery/', views.gallery, name='gallery'),
    path('social_media/', views.social_media, name='social_media'),
]