from django.shortcuts import render

# Create your views here.

def index(request): 
    return render(request,'home.html')

def about_me(request):
    return render(request, 'about_me_page.html')

def skill(request):
    return render(request, 'skill_page.html')

def gallery(request):
    return render(request, 'gallery.html')

def social_media(request):
    return render(request, 'socialmedia_page.html')
